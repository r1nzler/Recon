#!/usr/bin/env python
import subprocess
import sys
import os
import recon
import argparse
from yamlcfg import YamlConfig
config = YamlConfig(path='/root/Projects/Recon/config.yml')
savepath = config.path

# TODO: split dirb into multiple requests if ports >1
parser = argparse.ArgumentParser()
parser.add_argument('-u', "--host", help="URL you wish to scan")
parser.add_argument('-p', "--port", nargs='+', help="Ports to scan")
parser.add_argument('-r', "--robots", help="Check for robots.txt and check for disallow's", default='robots.txt', action ='store_true')
args = parser.parse_args()
port = ",".join(args.port)
ip_address = args.host

#todo
# if len(sys.argv) != 3:
#     print "Usage: httprecon.py <ip address> <port>"
#     sys.exit(0)

# ip_address = sys.argv[1].strip()
# port = sys.argv[2].strip()
if str(port) == "443":
    header = "https://"
else:
    header = "http://"

try:
    # print ('\033[1;37m[-]  ----------------------------------------------------------------------------- \033[1;m')
    # print('\033[1;37m[-]  |     Starting HTTP script scan for {0}:{1} \033[1;m'.format(ip_address, port))
    # print ("\033[1;37m[-]  ----------------------------------------------------------------------------- \033[1;m")

    if not recon.checknmaprunmod(ip_address, "_http.nmap.{0}".format(port, ip_address)):
        HTTPSCAN = "nmap -sSV -Pn -vv -p {0} --script-args=unsafe=1 --script='http* AND NOT broadcast AND NOT dos AND NOT http-slow* AND NOT http-comment*' --stats-every 10s -oN  {2}/{1}_http.nmap.{0} {1} -oX {2}/{1}_http.nmap.{0} {1}".format(port, ip_address, savepath)
        print(port)
        print(HTTPSCAN)
        results = subprocess.check_output(HTTPSCAN, shell=True)
        recon.logparsertxt(results)
        outfile = "{1}/{0}_HTTPrecon.txt".format(ip_address,savepath)
        f = open(outfile, "w")
        f.write(results)
        f.close()
    else:
        print('\033[1;33m[+]  {0} already scanned for HTTP port {1}...\033[1;m'.format(ip_address, port))
        results = open("{2}/{0}/{0}_http.nmap.{1}".format(ip_address, port,savepath), "r")
        recon.logparserfile(results)

  #  print "\033[1;37m[-]  ----------------------------------------------------------------------------- \033[1;m"
  #  print('\033[1;37m[-]  |     Starting Selenium ScreenGrab scan for {0}:{1} \033[1;m'.format(ip_address, port))
  #  print "\033[1;37m[-]  ----------------------------------------------------------------------------- \033[1;m"
  #  recon.screenshot_http(ip_address, port, header)
  #  print('\033[1;33m[+]  Added screenshot to results for {0}:{1}...\033[1;m'.format(ip_address, port))

    NIKTOSCAN = "nikto -host {0} -p {1} -C all -o {2}/{0}.{1}_nikto.txt".format(ip_address, port,savepath)
    # print ("\033[1;37m[-]  ----------------------------------------------------------------------------- \033[1;m")
    # print('\033[1;37m[-]  |     Starting NIKTO scan for {0}:{1} \033[1;m'.format(ip_address, port))
    # print ("\033[1;37m[-]  ----------------------------------------------------------------------------- \033[1;m")
    resultsnikto = subprocess.check_output(NIKTOSCAN, shell=True)
    recon.logparsernikto(resultsnikto)

    SSLSCAN = "sslscan --no-colour {0}:{1}".format(ip_address, port)
    # print ("\033[1;37m[-]  ----------------------------------------------------------------------------- \033[1;m")
    # print('\033[1;37m[-]  |     Starting SSL scan for {0}:{1} \033[1;m'.format(ip_address, port))
    # print ("\033[1;37m[-]  ----------------------------------------------------------------------------- \033[1;m")
    resultsssl = subprocess.check_output(SSLSCAN, shell=True)
    recon.logparsertxt(resultsssl)
    out = "/{2}/{0}.{1}_sslscan.txt".format(ip_address, port,savepath)
    print ('\033[1;33m[+]  report written to: {0}\033[1;m'.format(out))
    f = open(out, "w")
    f.write(resultsssl)
    f.close()

    ARACHNI = "arachni {0}{1}:{2} --output-only-positives --scope-include-subdomains --report-save-path {3}/{1}_{2}_arachni.bin'".format(header, ip_address, port,savepath)
    # print ("\033[1;37m[-]  ----------------------------------------------------------------------------- \033[1;m")
    # print('\033[1;37m[-]  |     Starting ARACHNI scan for {0}:{1} \033[1;m'.format(ip_address, port))
    # print ("\033[1;37m[-]  ----------------------------------------------------------------------------- \033[1;m")
    results2 = subprocess.check_output(ARACHNI, shell=True)
    recon.logparsernikto(results2)

    outfile2 = "{2}/{0}_Arachnirecon_{1}.txt".format(ip_address, port, savepath)
    f = open(outfile2, "w")
    f.write(results2)
    f.close()

    DIRBUST = "./Modules/dirbust.py {2}{0}:{1} {0} {1}".format(ip_address, port, header)  # execute the python script
    subprocess.call(DIRBUST, shell=True)

except:
    print('\033[1;31m[-]  HTTP script scan for {0}:{1} had some errors.\033[1;m'.format(ip_address, port))
os.system('stty echo')

